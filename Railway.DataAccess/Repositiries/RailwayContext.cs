﻿using System.Data.Entity;
using Railway.Models;

namespace Railway.DataAccess.Repositories
{
    public class RailwayContext : DbContext
    {
        public RailwayContext() : base("DBRailway")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RailwayContext>());
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

    }
}
