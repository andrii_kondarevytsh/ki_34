﻿using System.Linq;
using System.Windows;
using Railway.DataAccess.Repositories;
using Railway.Models;

namespace Railway
{
    public partial class TicketList : Window
    {
        public TicketList(Client client)
        {
            InitializeComponent();
            using (RailwayContext context = new RailwayContext())
            {
                var tickets = context.Tickets.ToList();

                foreach (Ticket ticket in tickets)
                {
                    if (client.Login == ticket.Login && client.Password == ticket.Password)
                    {
                        TicketBlock.Text += " From: " + ticket.From + "    To: " + ticket.Destination + "    Distance: " + ticket.Distance + "\n Arrival: " + ticket.Arrival + "    Departure: " + ticket.Departure + "\n Seat: " + ticket.Seat + "    Coach: " + ticket.Coach + "\n Price: " + ticket.Price + "grn\n\n";
                    }
                }

            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


    }
}
