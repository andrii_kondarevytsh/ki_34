﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Railway.Models;
using Railway.DataAccess.Repositories;

namespace Railway
{
    /// <summary>
    /// Interaction logic for BuyWindow.xaml
    /// </summary>
    public partial class BuyWindow : Window
    {
        public BuyWindow(Client client)
        {
            Client = client;
            InitializeComponent();
        }

        Client Client { get; set; }

        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            if (To.Text.Length == 0 || From.Text.Length == 0 || Date.Text.Length == 0 || BankCard.Text.Length == 0)
            {
                MessageBox.Show("Fill all fields!");

            }
            else
            {

                using (RailwayContext repository = new RailwayContext())
                {
                    Random rand = new Random();
                    Ticket ticket = new Ticket(Client, To.Text, From.Text, rand.Next() % 100 + 1, rand.Next() % 20 + 1, rand.Next() % 1344 + 100, Date.Text + $" {rand.Next() % 12}:{rand.Next() % 60}", Date.Text + $" {rand.Next() % 24}:{rand.Next() % 60}");
                    repository.Tickets.Add(ticket);

                    repository.SaveChanges();
                    MainWindow main = new MainWindow(Client);
                    main.Show();
                    Close();
                }
                Close();
            }
        }
    }
}
