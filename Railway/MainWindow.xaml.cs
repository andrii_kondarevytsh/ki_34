﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Railway.DataAccess.Repositories;
using Railway.Models;

namespace Railway
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(Client client)
        {
            Client = client;
            InitializeComponent();
        }

        public Client Client { get; set; }

        private void Show_Click(object sender, RoutedEventArgs e)
        {
            TicketList ticketList = new TicketList(Client);
            ticketList.Show();
        }

        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            BuyWindow buyWindow = new BuyWindow(Client);
            buyWindow.Show();
            Close();
        }

        private void Book_Click(object sender, RoutedEventArgs e)
        {
            BookWindow bookWindow = new BookWindow(Client);
            bookWindow.Show();
            Close();
        }
    }
}
