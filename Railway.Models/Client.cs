﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Railway.Models
{
    public class Client
    {
        public Client(string firstName, string lastName, string phone, string login, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Login = login;
            Password = password;
        }

        public Client()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }


    }


}
