﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Railway.Models
{
    public class Ticket
    {
        public Ticket(Client client, string destination, string from, int seat, int coach, int distance, string arrival, string departure)
        {
            Login = client.Login;
            Password = client.Password;
            Destination = destination;
            From = from;
            Seat = seat;
            Coach = coach;
            Distance = distance;
            Arrival = arrival;
            Departure = departure;
            Price = Distance * 0.27;
        }

        public Ticket()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Destination { get; set; }

        public string From { get; set; }

        public int Seat { get; set; }

        public int Coach { get; set; }

        public int Distance { get; set; }

        public string Arrival { get; set; }

        public string Departure { get; set; }

        public double Price { get; set; }
    }
}
